/**
 * Show all first base4h numbers with 1, 2, 3 or 4 digits.
 */
'use strict';
const b4h = require('./Base4h.js');
const bigInt = require('big-integer') // npm i big-integer

console.log("bits\tBinary\tBase4\tHexPh")
showFromCounters(8)


// LIB:

function showFromCounters(maxlength, cur = ''){
  // ver também https://stackoverflow.com/a/54506574/287948
  if (cur.length >= maxlength) return
  for (let i = 0; i < 2; i++) {
    //console.log(cur.length + 1 + ":",   cur + i)
    let bits = cur.length+1
    let next  = cur + i
    b4h.fromString(next)
    console.log( bits+"\t'"+next+"\t'"+b4h.toStringCanonic()+"\t'"+b4h.toStringCanonic2() )
    showFromCounters(maxlength, next)
  }
}
