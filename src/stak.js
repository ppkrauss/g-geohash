callManyTimes([2,2,2], show);

function callManyTimes(maxIndices, func) {
    doCallManyTimes(maxIndices, func, [], 0);
}

function doCallManyTimes(maxIndices, func, args, index) {
    if (maxIndices.length == 0) {
        let x = args.slice(0); // cloning
        while(x.length>0) {
          func(x); // why send array[array]?
          x.shift();
        }
    } else {
        var rest = maxIndices.slice(1);
        for (args[index] = 0; args[index] < maxIndices[0]; ++args[index]) {
            doCallManyTimes(rest, func, args, index + 1);
        }
    }
}

// ... function show()
function show(...args) {
   if (typeof args[0] == 'object') args=args[0] // workaround... can optimize?
   let code = String( args.reduce( (ac,cur) => ''+ac+cur ) )
   console.log( code.length, code )
}
