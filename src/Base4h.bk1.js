/**
 * Base4h class. Manages Base4-halfDigit variant of base4 and canonical representation base16plusHalf.
 * Depends on bigInt.
 */
'use strict';
const bigInt = require('big-integer') // npm i big-integer

const Base4h = {
  id:null,
  TWO: bigInt(2),
  TR_b16: { "0":"G","1":"H",
    "00":"I","01":"J","10":"K","11":"L",
    "000":"M","001":"N","010":"O","011":"P","100":"Q","101":"R","110":"S","111":"T" }
};
// duas representações canônicas: base4h e base16ph.


Base4h.bigint_toStringB4h = function(id, b16plusHalf=false) {
  if (typeof id != 'object') id=bigInt(id) // pode ser Geocode ou 'bigint' , BigInt.asUintN()
  if (id.lesserOrEquals(0)) return null;
  let bits = id.bitLength()
  if (b16plusHalf) {  // -- -- -- --  base16plusHalf  -- -- -- --
  let cut_rest = bits% 4;  // number of base16 excluded bits
  let cut = bits - cut_rest; // id prefix length, for base16
  if (cut_rest) {
    let mask = this.TWO.pow(cut_rest).prev();
    var s_cutrest = id.and(mask)
    var s0 = cut? id.shiftRight(cut_rest): null;
  } else {
    var s_cutrest = null
    var s0 = cut? id: null;
  }
  let r_hex = s0? s0.toString(16).padStart(cut/4,'0'): '';
  return r_hex+(s_cutrest? this.TR_b16[s_cutrest.toString(2).padStart(cut_rest,'0')]: '');
 } else {  // -- -- -- --  base4h  -- -- -- --
  let cut_rest = bits% 2;  // number of base4 excluded bit
  let cut = bits - cut_rest; // bits for usual base4
  if (cut_rest) {
    var s_cutrest = id.and(bigInt.one)
    var s0 = cut? id.shiftRight(1): null;
  } else {
    var s_cutrest = null
    var s0 = cut? id: null;
  }
  let r_b4 = s0? s0.toString(4).padStart(cut/2,'0'): '';
  return r_b4+(s_cutrest? this.TR_b16[s_cutrest.toString(2).padStart(cut_rest,'0')]: '');
 }
}

//  ==================

// test:

for(let i=0; i<1000; i++)
  console.log("\t", i+"=", bigInt(i).toString(2) +" = ",
      Base4h.bigint_toStringB4h(i) +" = ",
      Base4h.bigint_toStringB4h(i,true)
  );


