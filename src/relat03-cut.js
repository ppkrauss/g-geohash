/**
 * Show cuts.
 * Outputs CSV with tabs and secure string columns to copy/paste.
 */
'use strict';
const b4h = require('./Base4h.js');
const bigInt = require('big-integer') // npm i big-integer

const b4h_clone = Object.create(b4h);
console.log("bits\tBinary\tBase4h\tcut2_Base4\tcut3_Bin")
showBase4hValues(8) // change to any number of bits... take care with output overflow.

// // //

/*
pendencias:
2. decidir arquitetura se separa ou não a base16h do controle de macrocélulas
3. testar com mapa
5. Gerar testes/planilhas de casos reais com S2geometry e Geohash pontos de referencia.
  ... e publicar! Não precisa de mais nada. Mostrar a célula é secundário.
  Importante é mostrar que existe solução de corte jogando para binário e depois reconstruindo.

4. mostrador e editor de coberturas! Todas em base16ph
*/

function showBase4hValues(maxBits, cur = ''){
  // see also https://stackoverflow.com/a/54506574/287948
  if (cur.length >= maxBits) return
  for (let i=0; i<2; i++) {
    let bits = cur.length+1
    let next  = cur + i
    b4h.fromString(next) // set b4h with the next binary sample.
    let val = b4h.toString('base4h')

    let suffix = ''
    let prefix = val.slice(0,2)
    if (prefix.length==2) {
      b4h_clone.fromString(prefix,'base4h')
      let aux = b4h.replace_strBinPrefix(b4h_clone.toString(2))
      if (aux)
        suffix += " = "+b4h_clone.fromString(aux).toString('base4h')
    } else prefix = '';

    let val2 = b4h.toString(2)
    let suffix2 = ''
    let prefix2 = val2.slice(0,3)
    if (prefix2.length==3) {
      b4h_clone.fromString(prefix2,2)
      let aux = b4h.replace_strBinPrefix(b4h_clone.toString(2))
      if (aux)
        suffix2 += " = "+b4h_clone.fromString(aux).toString('base4h')
    } else prefix2 = '';

    console.log(
      bits
      +"\t'"+next
      +"\t'"+val
      +"\t'-"+prefix+suffix
      +"\t'-"+prefix2+suffix2
      //+"\t'"+b4h.toString(32)
    )
    showBase4hValues(maxBits, next)
  }
}
