/**
 * Show all first Base4h values with 1, 2, 3 or 4 digits.
 * Outputs CSV with tabs and secure string columns to copy/paste.
 * To assert: diff result ../data/relat02-canoniCodes.csv
 */
'use strict';
const b4h = require('./Base4h.js');
const bigInt = require('big-integer') // npm i big-integer

console.log("bits\tBinary\tBase4h\tHexPh")
showBase4hValues(8) // change to any number of bits... take care with output overflow.

// // //

function showBase4hValues(maxBits, cur = ''){
  // see also https://stackoverflow.com/a/54506574/287948
  if (cur.length >= maxBits) return
  for (let i=0; i<2; i++) {
    let bits = cur.length+1
    let next  = cur + i
    b4h.fromString(next) // set b4h with the next binary sample.
    console.log(
      bits
      +"\t'"+next
      +"\t'"+b4h.toString('base4h')
      +"\t'"+b4h.toString('base16ph')
    )
    showBase4hValues(maxBits, next)
  }
}
