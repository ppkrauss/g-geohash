/**
 * Base class. Manages usual (2, 4, 16 and 32) bases and Base4-halfDigit variants.
 * Depends on bigInt.
 */
'use strict';
const bigInt = require('big-integer') // npm i big-integer


var Base = {  // or const?  will be exported...

  // instance CONFIGS:
   cf_alphabetLabel: 'ghs'  // OUTPUT base. old cf_hash_baseAlphabetLabel
  ,cf_halfDigit0: 'A' // or top_black '⬒'
  ,cf_halfDigit1: 'B' // or down_black '⬓'
  ,cf_renderSep: '.' // standard is empty. Usual '.'. Old cf_hash_sep
  ,cf_renderGroupRgx: /([^\+\-\.,;]{3})/g  // f(alphabet,cf_renderSep) usual or any other, for clean hash.

  // CACHED-CONFIGS: obtained by method this.config() constructor
  ,kx_halfDigit_2to4:{"0":"A","1":"B"} //f(cf_halfDigit0,cf_halfDigit_1)
  ,kx_halfDigit_2to4:{"A":"0","B":"1"} // idem
  ,kx_alphabetCase: 'lower'
  ,kx_alphabet: '0123456789bcdefghjkmnpqrstuvwxyz' // f(cf_alphabetLabel)
  ,kx_radix: 32
  ,kx_radixBits:5  // factor to length fill zeros, bits/2
  ,kx_halfDigit_Detect: /[AB]$/  // or /[⬒-⬓]$/u  // f(cf_halfDigit0,cf_halfDigit1)
  ,kx_b4h_Detect: /^([0-3]+)([AB])?$/  // or /^([0-3]+)([⬒-⬓])?$/u
  ,kx_halfLevel_isValid: true // indica se permitido ou não na base OUTPUT.
};

Base.alphabetOptions = { // for Base.config and hlp_*convertBase.
  'js':   '0123456789abcdefghijklmnopqrstuv' // standard ISO or Javascript (ECMA-262)
  ,'ghs': '0123456789bcdefghjkmnpqrstuvwxyz' // standard Geohash (js except "ailo")
  ,'pt':  '0123456789BCDFGHJKLMNPQRSTUVWXYZ' // no frequent vowel of Portuguese (for non-silabic codes)
  ,'rfc': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567' // RFC3548
}
//Base.alphabetOptions_case = {'ghs':'lower', 'pt':'upper', 'rfc':'upper', 'js':'lower'}
// use false for non-caseSensitive

/**
 * Cache constructor, for faster base convertions in base2 and base4 strings.
 * Faster and independent from classes bigInt or Number.
 */
Base.pair_build = function (b) {
    var a = Math.log(b)/Math.log(2);  // So a,b = 2,4 or 4,16.
    var p_ab = 'pair_'+a+'to'+b;
    var p_ba = 'pair_'+b+'to'+a;
    Base[p_ab] = {};
    Base[p_ba] = {};
    for (var i=0;i<a;i++) for(var j=0;j<a;j++) {
      var q = String(i)+String(j);
      var h = (i*a+j).toString(b); // standard JS for little integers
      Base[p_ab][q] = h;
      Base[p_ba][h] = q;
  }
};
Base.pair_build(4);  // constructor of pair_4to2 and pair_2to4.
Base.pair_build(16); // constructor of pair_16to4 and pair_4to16.
// run configs!

// MAIN METHODS:

/**
 * (old hlp_hashSantize)
 * Sanitize hash input or hash_rendered.
 * The cleanHalfDigit parameter is only to external use.
 * @return sanitized string (without affect props)
 */
Base.santize = function(s,removeHalfDigit) {
  if (removeHalfDigit && this.kx_halfLevel_isValid && this.base==4) {
    if (removeHalfDigit===2) // truncates the hash, from level L+1/2 to L.
      s = s.replace(this.kx_halfDigit_Detect,'');
    else if (removeHalfDigit===true || removeHalfDigit===1) // emulates hash of level L+1
      s=s.replace(this.cf_halfDigit0,'0').replace(this.cf_halfDigit1,'2');
  }
  return s.replace(/[\.\-:;,]+/g,''); // preserves "+" of plusCodes
}; // \func


////////////
////// base convertion to be tested by e.g. https://jsperf.com


/**
 * (old hlp_outBase_check)
 * Ckeck for valid tring type.
 */
Base.outBase_check = function (x) {
  // !Merge with hlp_hash_isValid()!?
  if (typeof x != 'string')
    throw new Error('ER10: expecting x as string.');
  x = x.trim();
  if (!x) throw new Error('ER11: expecting non-empty x.');
  return x;
}

// hlp_baseX_to_baseY convertions:

/**
old hlp_base4_to_outBase
LIXO, ver _4h_to_16
 * Converts base4 number (string) into outBase (string).
 * The outBase is expressed in the alphabet indicated by this.cf_alphabetLabel,
 * and its length proportional to this.level, padding zeros.
 * @param x string, input
 */
Base.b4_to_bOut = function(x) {
  if (!x || typeof x != 'string') throw new Error('ER15: Base4 MUST be string and non-empty');
  var outBase_len = x.length*2/this.kx_radixBits;
  return bigInt(x,4,'0123',true)
        .toString(this.kx_radix,this.kx_alphabet)
        .padStart(outBase_len,'0');
};

/**
 * (old hlp_base4h_to_outBase)
 * Converts base4h number (string) into outBase (string).
 * The outBase is expressed in the alphabet indicated by this.cf_alphabetLabel,
 * and its length proportional to this.level, padding zeros.
 * @param x string, input
 */
Base.b4h_to_bOut = function(xB4h,xB4h_level) {
  var good=String(xB4h).trim().match(this.kx_b4h_Detect); // utf8 must be a range.
	if (!good)
    throw new Error('ER16: Base4h MUST be string and non-empty');
	var lastHalfDigit = good[2]? good[2]: '';
	var lastBit=''; // will be '', '0' or '1'.
  if (lastHalfDigit) lastBit = (lastBit==this.cf_halfDigit0)? '0': '1';
	var xB4 = good[1];
  if (lastHalfDigit) {
    var x_bin =  bigInt(xB4,4,'0123').toString(2)+lastBit;
    return bigInt(x_bin,2).toString(this.kx_radix, this.kx_alphabet)
          .padStart(xB4h_level*0.4,'0');
  } else
    return bigInt(xB4,4,'0123',true)
          .toString(this.kx_radix, this.kx_alphabet)
          .padStart(xB4h_level*0.4,'0'); // usar kx* para 2/5, etc.
};

Base.reduce_b4_to_b4h = function(x) { // old hlp_base4_to_b4half
  // lost the last bit changing base4Digit to halfDigit.
  x = String(x).replace(/[01]$/,this.cf_halfDigit0).replace(/[23]$/,this.cf_halfDigit1);
  return x;
};

/**
 * (old hlp_outBase_to_base4h)
 * Converts outBase number (string) into base4 (string).
 * The outBase is expressed in the alphabet indicated by this.cf_alphabetLabel,
 * and its length proportional to this.level, padding zeros.
 OPS, gerar fração da base4 requer mais um bit!
 * @param x string, input
 */
Base.bOut_to_b4h = function (x) {
  //console.log("debug10.1: bOut_to_b4h x=",x )
  x = this.outBase_check(x);
  this.hlp_tmp = x.length*this.kx_radixBits/2; // base4h_len=level
  if (this.base==4)
    return x;
  var r = bigInt(x,this.kx_radix,this.kx_alphabet,true);
  if (Number.isInteger(this.hlp_tmp))
    return r.toString(4).padStart(this.hlp_tmp,'0');
  else {
    // ugly, use bitwise!
    var r_bin = r.toString(2).padStart(this.hlp_tmp*2,'0')+"0"; // add 1 bit
    r = bigInt(r_bin,2).toString(4).padStart(this.hlp_tmp+1,'0');
    r = this.reduce_b4_to_b4h( r );  // removes 1 bit
    return r;
  }
};

Base.b4h_halfDigitSplit = function(x) {
  x=String(x);
  if (x===undefined || x===null) alert("BUG1 EM b4h_halfDigitSplit()")
  if (!x.match(this.kx_halfDigit_Detect))
    return [x];
  var exoticDigits = {};
  exoticDigits[this.cf_halfDigit0] = [0,1];
  exoticDigits[this.cf_halfDigit1] = [2,3];
  var hashLastDigit = x.slice(-1);
  var hashMain = x.slice(0,-1);
  var parts=[];
  for(var i=0;i<2;i++)
    parts[i] = hashMain + exoticDigits[hashLastDigit][i];
  return parts;
}; // \func

// hlp_int_to_* convertions:

/**
 * Converts integer number (internal binary) into base32 (string representation).
 * The base32 is expressed in the alphabet indicated by this.cf_alphabetLabel.
 * @param x Number or bigInt, input
 * @param usePad boolean (dft true), for padding zeros by this.level
 */
Base.hlp_int_to_outBase = function (x,usePad=true) {
  if (typeof x =='number')
    x = bigInt(x);
  else if (typeof x != 'object') //  bigInt object
    throw new Error('ER18: expecting bigingt or integer parameter, supplied '+(typeof x));
  var outBase_len = this.level*2.0/this.kx_radixBits; // supposing, not use padStart
  x = x.toString(this.kx_radix,this.kx_alphabet);
  return usePad? x.padStart(outBase_len,'0'): x;
}

/**
 * Converts integer number (internal binary) into base4 (string representation).
 * The inverse function is Javascript parseInt(x,4).
 * @param x integer, input
 * @param len number, requested length (integer or float), padding zeros.
 */
Base.hlp_int_to_base4 = function (x,len) {
  // for notInt len, see bOut_to_b4h()
  if (typeof x =='number')
    x = bigInt(x);
  else if (typeof x != 'object') //  bigInt object
    throw new Error('ER20: expecting bigint or int parameter of int_to_base4, but supplied '+(typeof x));
  return x.toString(4).padStart(len,'0');
}; // \func

///

/**
 * Converts base4h (with halfBigit) to binary, returning information about original level.
 */
Base.hlp_base4h_to_bin = function (xB4h) {
  // future kx_regex must use kx_halfDigit_Detect
	var good=xB4h.match(/^([0-3]+)([⬒-⬓])?$/u); // utf8 must be a range.
	if (!good)
		return null;
	var lastHalfDigit = good[2]? good[2]: '';
	var lastBit=''; // will be '', '0' or '1'.
  if (lastHalfDigit) lastBit = (lastBit=='⬒')? '0': '1';
	var xB4 = good[1];
	var b4_len = xB4.length;
	var b2_len = b4_len*2 + ((lastBit==='')?0:1);
	var xB2 = (bigInt(xB4,4).toString(2) + lastBit).padStart(b2_len,'0');
	return { b2:xB2, level:b4_len+lastHalfDigit?0.5:0 }
}; // \func

/**
falta remover bigInt, use conversão string 00=0,01=1...
 * Cut head (prefix) of x in binary string and returns tail as base4j string.
 * @param p string base4h input prefix
 * @param x string base4h main input
 * @return array [base4h tail, float level]
 */
Base.hlp_base4h_cutPrefix = function (p,x) {
	p = this.hlp_base4h_to_bin(p);
	x = this.hlp_base4h_to_bin(x);
	if (!p || !x) return null; // error, bad number
	if (p.level>x.level) return null; // error, impossible p to be a root
	var x_root = x.b2.slice(0,p.b2.length);
	if (x_root!=p.b2) return null; //error not same root
	var resB2_len = x.b2.length - p.b2.length; // se nao for divisivel por 2 nao pode ser base4, guardar o ultimo bit!
	var res = x.b2.slice(p.b2.length);
	if (resB2_len!=res.length) console.log("bug5")
	var resB4h_len = resB2_len/2.0;
	if (Number.isInteger(resB4h_len))
		return [bigInt(res,2).toString(4).padStart(resB4h_len,'0'),resB4h_len];
	else {
		var btoh = {"0":"⬒","1":"⬓"};
		var bit = res.slice(-1);
		var res = res.slice(0,-1);
    //console.log("res2=",res)
		var len = Math.floor(resB4h_len);
		return [bigInt(res,2).toString(4).padStart(len,'0') + btoh[bit], resB4h_len];
	}
}; // \func

/**
 * Changes a hash string to correct case or empty when invalid.
 * Generates alert() when showAlert flag.
 * Can be used with all default parameters, that is the same as a=true,
 * Other uses: with string a=geohash or boolean a=false or a=geohash and b=false.
 * @param retHash boolean false. INTErNAL USE.
 * @return boolean.
 */
Base.hlp_hash_isValid = function(a,b,retHash=false) {
  var showAlert=true;
  var hash = '';
  if (a!==undefined) {
     if (typeof a === 'boolean') showAlert=a;
     else { // int or string
	   hash = a;
           if (b!==undefined && !b) showAlert=false;
     }
  } // else using hash and showAlert.
  var rgx = new RegExp('^['+this.kx_alphabet+']+$');
  if (!hash || hash.length === 0) {
     if (showAlert) alert("Empty Base");
     return retHash? null: false;
  } else {
    hash = this.santize(hash);
    // Normalize case:
	  if (this.kx_alphabetCase=='lower')
	    hash = hash.toLowerCase();
	  else if (this.kx_alphabetCase=='upper')
	    hash = hash.toUpperCase();
	  var r = hash.match(rgx); // VALIDATION
    if (!r && showAlert)
      alert("The string \n"+hash+"\n is not valid!\n All letters MUST be in \n'"+this.kx_alphabet+"'");
	  return retHash? (r? hash: null): r;
  }
}; // \func

////// BASE TOOLS:


// // //
Base.b4h_to_b16h = function(x) {
  if (typeof x !='string') throw new Error('Base ER01: must be string');
  var s = x.match(/([0-3AB]){1,2}/g);
  var lst = s.pop();
  if (lst.length==2 && lst.charCodeAt(lst.length-1)<60) { // same as lst.match(/^[0-3]{2,2}$/)
    s.push(lst); lst='';
  }
  // supposing faster tham parserInt(s[i],4).toString(16) them s.join()
  for(var i=0, s16='';i<s.length;i++) {s16+=Base.pair_4to16[s[i]];}
  return  s16+(lst?('+'+lst):'');
}; // \func
Base.b16h_to_b4h = function(x) {
  if (typeof x !='string') throw new Error('Base ER01: must be string');
  var lst = '';
  var m=x.match(/^([^\+]*)\+(.+)$/)
  if (m) {
    lst = m[2];
    x = m[1]; // must be string
  }
  var fst = '';
  if (x) // supposing faster tham (x? bigInt(x,16).toString(4): '')
    for(var i=0; i<x.length; i++)
      fst+= Base.pair_16to4[x.charAt(i)];
  return fst+lst;
}; // \func

// // //
Base.b2_to_b4h = function(x) {
  if (typeof x !='string') throw new Error('Base ER01: must be string');
  var s = x.match(/([0-1]){1,2}/g);
  var lst = s.pop();
  if (lst.length==2) {
    s.push(lst); lst='';
  } else
    lst = Base.kx_halfDigit_2to4[lst];
  for(var i=0, s4=''; i<s.length; i++) {s4+=Base.pair_2to4[s[i]];}
  return  s4+(lst?('+'+lst):'');
}; // \func
Base.b4h_to_b2 = function(x) {
  if (typeof x !='string') throw new Error('Base ER01: must be string');
  var last = '';
  var m=x.match(/^([^AB]*)([AB])$/)
  if (m) {
    last = Base.kx_halfDigit_2to4[m[2]];
    x = m[1];
  }
  var first = '', c;
  if (x) // supposing faster tham (x? bigInt(x,4).toString(2): '')
    for(var i=0; i<x.length; i++)
      first+= ((c=Base.pair_4to2[x.charAt(i)])==undefined)? '': c;
  return first+last;
}; // \func

Base.b2_to_b16h = function(x) { // non-optimized, but non-used
  return Base.b4h_to_b16h(Base.b2_to_b4h(x)); // not use this.method to reuse contructor
};
Base.b16h_to_b2 = function(x) { // non-optimized, but non-used
  return Base.b4h_to_b2(Base.b16h_to_b4h(x));  // not use this.method to reuse contructor
};



/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
if (typeof module != 'undefined' && module.exports)
    module.exports = Base; // CommonJS, node.js
