'use strict';
const bigInt = require("big-integer"); // npm i big-integer
const fs     = require('fs')
const path   = require('path')
const parse  = require('csv-parse/lib/sync') // npm i csv-parse  (not parse"r")
// const jszip = require('jszip')  // npm i jszip

// CONFIGS:
const cf_cover_dataset = '../data/cover.csv'
const cf_bases_dataset = '../data/catalog-base.csv'

var COVER = null;

console.log("debug....")
for(var i=0;i<300; i++)
  console.log(  " ", i+"  _dec",
     bigInt(i).toString(16)+"  _hex =", int_to_base16plus2_bin(i),
     " -- "+bigInt(i).toString(2)+"  _bin"+bigInt(i).bitLength()
  );



// testing:
console.log("\n-----\nVisualizando códigos de cobertura em base16plus2");
for(let bits=4; bits<10; bits++) for(let id=30; id<35; id++)
   console.log( bits+" bits: ", id+"=", int_to_base16plus2(id,bits),'==',int_to_base16plus2_bin(id,bits) );


console.log( "\n-------\nBases de exemplo:" );
var bases = datasetLoad(cf_bases_dataset,convCsvHeader_toObj);
var lsBase = [4,16,32,64];
var lsSample = [0,3,4,10,11,12,15,16,33,60,63,65,90]
for( let b of Object.keys(bases) ) {
  let thisBase = bases[b];
  let bb = Number(thisBase.base);
  if (thisBase.alphabet && lsBase.includes(bb)) {
    console.log( thisBase );
    for (let i of lsSample) {
      let x = bigInt(i).toString(bb,thisBase.alphabet);
      console.log( "\t",i,"=", x );
    }
  }
}

// LIXOS:
// console.log( JSON.stringify(x) );
//console.log( Object.keys(bases) );
// var COVER2 = datasetLoad(cf_cover_dataset,convStdTable);
// console.log( JSON.stringify(COVER2) );



// Tabela 1 - Alinhamentos!
console.log( "\n-------\nAlinhamentos:" );

lsBase = ['base4','base16','base32hex','base64url'];
lsSample = [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40]
for (let odd=0;odd<2;odd++) {
  let s=odd? ' ': '';
  for(let i of lsSample) s=s+' '+((i+odd)%10)
  console.log(s," bits") // mapear para zero a 8, ou seja, %8
  for(let b of lsBase) {
    let thisBase = bases[b];
    let s=odd? ' ': '';
    for(let i of lsSample) s=s+' '+((((i+odd)%thisBase.bits)==0)? "|":" ")
    // thisBase.bits bate com número de bits em i? basta i%
    if (s.trim()) console.log(s,"="+thisBase.base)
  }
}

console.log( "\n-------\nTeste:" );

///////////////
// lib

function int_to_base16plus2(id,bits) {
  // algoritmo STRING
   let s0 = id.toString(2).padStart(bits,'0');
   let cut = Math.floor(bits/4.0)*4;
   let r_cut = s0.slice(cut);
   if (r_cut) r_cut = "+"+r_cut;
   let r_hex = parseInt(s0.slice(0,cut),2).toString(16).padStart(bits/4,'0');
   return r_hex+r_cut;
}

function int_to_base16plus2_bin(id,bits) {  // plushalf
  // algoritmo binário, máscara pega final do número.... ver seu not()
  const tr = { "0":"G","1":"H",
    "00":"I","01":"J","10":"K","11":"L",
    "000":"M","001":"N","010":"O","011":"P","100":"Q","101":"R","110":"S","111":"T" }

  let id0 = bigInt(id); //.shiftLeft(1)  // drop signal bit
console.log(typeof id0);

  if (id0.lesserOrEquals(0)) return null;
  //if (bits) id0 = id0.and( bigInt( ''.padStart(bits,'1'), 2 ) );
  // faster? tham
  bits = id0.bitLength();  // na base4 isDegen = (bits & 1)
  let cut_rest = bits%4;  // base16
  let cut = bits - cut_rest;
  if (cut_rest) {
    let mask = bigInt(2).pow(cut_rest).prev(); // == bigInt(''.padStart(cut_rest,'1'),2)
    var s_cutrest = id0.and(mask)
    var s0 = cut? id0.shiftRight(cut_rest): null;
  } else {
    var s_cutrest = null
    var s0 = cut? id0: null;
  }
  let r_hex = s0? s0.toString(16).padStart(cut/4,'0'): '';
  return r_hex+(s_cutrest? tr[s_cutrest.toString(2).padStart(cut_rest,'0')]: '');
}


function convStdTable(x) {
  // STD = 0'municipio', 1'ref', 2'celula_b4h', 'nivel', 'notas'
  if (x[0][0]=='municipio')
    x.shift();  //  drops CSV header
  let current = null; // municipio
  let lst = {};
  for (let i of x) {
    if (i[0]!=current) lst[ current=i[0] ] = [];
    let cell = i[2];
    if (cell.charAt(0)=='x') cell = cell.substr(1)
    if (cell) lst[current].push(cell);
  }
  return lst
}

function datasetLoad(fpath,csvConvert) {
  var file = path.resolve(__dirname, fpath);
  if (path.extname(file)=='.csv') {
    console.log("debug 1. dataset from CSV");
    let raw = parse(fs.readFileSync( file ), {
      trim: true,
      skip_empty_lines: true
    });
    return  (csvConvert===undefined)? raw: csvConvert(raw);
  } else {
    console.log("debug 1. dataset from JSON");
    return JSON.parse(file)  //  await !
  }
} // \func


function convCsvHeader_toObj(x,useId='id') {
  let h = x.shift();  // CSV header
  if (h.includes(useId))
    var lst = {};
  else {
    var lst = [];
    useId = '';
  }
  for (let i of x) {
    let tmp = {};
    for (let j of h) tmp[j]=i.shift();
    if (useId)
      lst[tmp[useId]] = tmp;
    else
      lst.push(tmp);  // CSV line
  }
  return lst
}
