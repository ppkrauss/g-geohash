/**
 * Base4h class. Manages Base4-halfDigit variant of base4
 * and canonical representation "hexadecimal plus-half", base16ph.
 * Depends on bigInt.
 */
'use strict';
const bigInt = require('big-integer') //  polyfill. npm i big-integer
// NOTICE: check possibility to change to standard native BigInt Javascript.

var Base4h = {
  bin:null, // main register, bigInt object
  bits:0,   // main register, number os bits (to pad zeros)

  TR_b16: {
    "0":"G","1":"H",
    "00":"I","01":"J","10":"K","11":"L",
    "000":"M","001":"N","010":"O","011":"P","100":"Q","101":"R","110":"S","111":"T"
  },
  TR_bin: {
    "G":"0","H":"1",
    "I":"00","J":"01","K":"10","L":"11",
    "M":"000","N":"001","O":"010","P":"011","Q":"100","R":"101","S":"110","T":"111"
  },
  rgxBase:{"base4h": /^([0123]*)([GH])?$/, "base16ph": /^([0-9a-f]*)([G-T])?$/},
  exoticAlphabet: { // for exoticAlphabet labels, see also data/catalog-base.csv
    'js':   '0123456789abcdefghijklmnopqrstuv' // RFC3548's base32hex = Javascript (ECMA-262)
    ,'ghs': '0123456789bcdefghjkmnpqrstuvwxyz' // standard Geohash (js except "ailo")
    ,'pt':  '0123456789BCDFGHJKLMNPQRSTUVWXYZ' // no frequent vowel of Portuguese (for non-silabic codes)
    ,'rfc': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567' // RFC3548 base32
    ,'url': 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_' // RFC sec. 5
  },
  stdAlphabet: { // see data/catalog-base.csv
    "base16":"js","base32":"js","base64":"url"
  }
};

/**
 * Set this with the specified input.
 * @param strval string valid number expressed in the baseOpt.
 * @param baseOpt string of a valid base label.
 * @return this for changing.
 */
Base4h.fromString = function(strval,baseOpt=2) {
  if (!strval)
    throw new Error("er01, empty value");
  let halfDigit = ''
  let conf = this.parseLabel(baseOpt) //{label,base,useHalfDigit,exoticAlphabet}
  if (conf.base==2) {
    if ( !strval.match(/^[01]+$/) ) throw new Error("er02, invalid synxtax for string-binary");
    this.bits = strval.length
  } else if ( [4,16,32,64].includes(conf.base) )  { // base4h or other!
    if (conf.label=='base4h' || conf.label=='base16ph') {
      let found = strval.match(this.rgxBase[conf.label])
      if (!found)  throw new Error("er03, invalid synxtax for "+conf.label+" value");
      let main = found[1]
      halfDigit = found[2]
      let halfBin = halfDigit? this.TR_bin[halfDigit]: ''
      this.bits = main.length*conf.bitsPerDigit + halfBin.length
      strval = bigInt(main,conf.base).toString(2) + halfBin
    } else {  // other base, no halfDigit representation
      if (!Number.isInteger(conf.bitsPerDigit))
        throw new Error("er04, invalid input base "+conf.base);
      this.bits = strval.length*conf.bitsPerDigit
      strval = bigInt(strval,conf.base).toString(2)
    }
  } else
    throw new Error("er05, invalid base "+conf.base);
  this.bin = bigInt(strval,2)
  if (this.bin.bitLength()>this.bits)
    throw new Error("er06, internal BUG, very strange!");
  return this;
};

/**
 * @return string representing the specified baseOpt.
 * @param baseOpt string of a valid base label.
 */
Base4h.toString = function(baseOpt='base16ph',errorOnInvalid=false) {
  let conf = this.parseLabel(baseOpt)
  if (conf.base==2)
    return this.toStringBin()
  else if (conf.useHalfDigit)
    return this.toStringPlusHalf()
  else {
    if (this.bits % conf.bitsPerDigit != 0) {
      if (errorOnInvalid)
        throw new Error("invalid binary length, must be a multiple of "+conf.bitsPerDigit);
      else return '';
    }
    let digits = this.bits / conf.bitsPerDigit
    if (conf.exoticAlphabet)
      return this.bin.toString(conf.base,conf.exoticAlphabet).padStart(digits,'0')
    else
      return this.bin.toString(conf.base).padStart(digits,'0')
  }
}

Base4h.toStringBin = function() { // canonic output.
  return this.bin.toString(2).padStart(this.bits,'0')
}

Base4h.toStringPlusHalf = function(conf=null) {  // canonic compressed alternatives.
  if (!this.bits) return '';
  if (!conf) conf = this.lastParseLabel; // when conf===undefined || conf===null
  else if (typeof conf != 'object') conf = this.parseLabel(conf,true);
  if (!conf.useHalfDigit) throw new Error("er01, invalid conf");

  let strBin = this.toStringBin()
  let halfDigit = ''
  let cut_rest = this.bits % conf.bitsPerDigit //conf.base;  // number of excluded bits
  let cut = this.bits - cut_rest; // bits in the main number
  if (conf.base==4) {
    if (cut_rest) {
      let b = this.bits-1;
      let last = strBin.charAt(b)
      strBin = b? strBin.substring(0,b): ''
      halfDigit = this.TR_b16[last]
    }
  } else if (cut_rest) { // non-zero when base16 non-exact
    let c = cut-1;
    let lasts = strBin.substring(cut) // from cut
    strBin = c? strBin.substring(0,cut): ''
    halfDigit = this.TR_b16[lasts]
  }
  let b = strBin? bigInt(strBin,2).toString(conf.base).padStart(cut/conf.bitsPerDigit,'0'): '';
  return b + halfDigit
};

Base4h.replace_strBinPrefix = function(strbin_prefix,strbin_new='') {
  let full = this.toStringBin()
  var rgx = new RegExp('^'+strbin_prefix+'(.+)$')
  let found = full.match(rgx)
  return found? (strbin_new+found[1]): ''
}


// // // //
// Extended user interface, must updated with datasets.

/**
 * Parses the base (radix) label descriptor.
 * @param l mix string, null, undefied or number, free-label
 * @returns {label,base,useHalfDigit,exoticAlphabet}
 * lixo: this.onFalseError(this.parseLabel(baseOpt))
 */
Base4h.parseLabel = function(l,forceIsHalf=false) {
  if (!l) l='base2';
  let t = typeof l
  if (t == 'number') l='base'+String(l)+(forceIsHalf? 'h':'');
  else if (t!='string')
    throw new Error("invalid type, must be number or string");
  l = l.toLowerCase()
  if (l=='base32hex') l='base32js'; // exceptions
  let pref3 = l.slice(0,3)
  if (pref3=='bin') l='base2';
  else if (pref3=='hex') l='base16'+l.slice(3);
  let m = l.match(/^b(?:ase)?(\d+)(.+)?$/)
  if (!m)
    throw new Error("invalid label syntax, must be 'baseXy'");
  let radix = Number(m[1])
  let alphabet = m[2]? m[2]: ''
  let useHalfDigit = false;
  if (m[2]=='ph' || m[2]=='h') {
    useHalfDigit = true
    alphabet = ''  // standard
  }
  if (alphabet=='std') alphabet='';
  var lab = 'base'+radix
  if (useHalfDigit) {
    if (radix==4) lab = lab+'h';
    else if (radix==16) lab = lab+'ph'; // ? change to h
    else
      throw new Error("invalid radix, must be 4 or 16  when using halfDigit");
  } else
    lab = lab+alphabet
  this.lastParseLabel = {
    label: lab
    ,base: radix
    ,useHalfDigit: useHalfDigit
    ,bitsPerDigit: Math.log(radix)/Math.log(2)
    ,exoticAlphabet: alphabet
  };
  return this.lastParseLabel;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
if (typeof module != 'undefined' && module.exports)
    module.exports = Base4h; // CommonJS, node.js
    //module.exports = Base4h;


//  ==================

/*old but ok!

const Base4h = {
  id:null,
  TWO: bigInt(2),
  TR_b16: { "0":"G","1":"H",
    "00":"I","01":"J","10":"K","11":"L",
    "000":"M","001":"N","010":"O","011":"P","100":"Q","101":"R","110":"S","111":"T" }
};
// duas representações canônicas: base4h e base16ph.


Base4h.bigint_toStringB4h = function(id, b16plusHalf=false) {
  if (typeof id != 'object') id=bigInt(id) // pode ser Geocode ou 'bigint' , BigInt.asUintN()
  if (id.lesserOrEquals(0)) return null;
  let bits = id.bitLength()
  if (b16plusHalf) {  // -- -- -- --  base16plusHalf  -- -- -- --
  let cut_rest = bits% 4;  // number of base16 excluded bits
  let cut = bits - cut_rest; // id prefix length, for base16
  if (cut_rest) {
    let mask = this.TWO.pow(cut_rest).prev();
    var s_cutrest = id.and(mask)
    var s0 = cut? id.shiftRight(cut_rest): null;
  } else {
    var s_cutrest = null
    var s0 = cut? id: null;
  }
  let r_hex = s0? s0.toString(16).padStart(cut/4,'0'): '';
  return r_hex+(s_cutrest? this.TR_b16[s_cutrest.toString(2).padStart(cut_rest,'0')]: '');
 } else {  // -- -- -- --  base4h  -- -- -- --
  let cut_rest = bits% 2;  // number of base4 excluded bit
  let cut = bits - cut_rest; // bits for usual base4
  if (cut_rest) {
    var s_cutrest = id.and(bigInt.one)
    var s0 = cut? id.shiftRight(1): null;
  } else {
    var s_cutrest = null
    var s0 = cut? id: null;
  }
  let r_b4 = s0? s0.toString(4).padStart(cut/2,'0'): '';
  return r_b4+(s_cutrest? this.TR_b16[s_cutrest.toString(2).padStart(cut_rest,'0')]: '');
 }
};

*/


/* test:
for(let i=0; i<5; i++)
  console.log("\t", i+"=", bigInt(i).toString(2) +" = ",
      Base4h.bigint_toStringB4h(i) +" = ",
      Base4h.bigint_toStringB4h(i,true)
  );


for (var i=0;i<2; i++) for (var j=0; j<2; j++) for (var k=0; k<2; k++) {
  let x = ""+i+j+k
  let c = CellID.iniByStr(x).show()
  console.log("\t"+x, c )
}
*/
