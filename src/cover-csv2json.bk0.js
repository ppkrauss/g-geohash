'use strict';
// future use https://github.com/frictionlessdata/datapackage-js
//const bigInt = require('big-integer') // npm i big-integer
const fs     = require('fs')
const path   = require('path')
// const jszip = require('jszip')  // npm i jszip

// CONFIGS:
const cf_cover_dataset = '../data/cover.csv'
var COVER = null;

// LOAD external configs:
  var COVER_file = path.resolve(__dirname, cf_cover_dataset);
  if (path.extname(cf_cover_dataset)=='.csv') {
    //const parse = require('csv-parse/lib/sync')
    const parse  = require('csv-parse') // npm i csv-parse  (not parse"r")
    let raw = []
    parse(fs.readFileSync( COVER_file ), {
      trim: true,
      skip_empty_lines: true
    })
    .on('readable', function(){
      let record
      while (record = this.read()){
        raw.push(record)  ; }
    })
    .on('error', function(err){ console.error(err.message) })
    .on('end', function(){  //  await here!
      COVER = convStdTable(raw);
      console.log("1. cover from CSV");
      main();
    });
  } else {
    COVER = JSON.parse(COVER_file)  //  await !
    console.log("1. cover from JSON");
    main();
  }

function main() {
  var COVER2 = {};
  console.log("2. Translating...");
  for (let municipio of Object.keys(COVER)) {
    COVER[municipio].sort( // faster tham a.localeCompare(b)
      (a, b) => ( a<b? -1 : +(a>b) ) || (a.length - b.length)
    );
    let prefix = COVER[municipio][0]; // ugly prefix-extractor, please reduce()
    for (let j=0; j<2; j++) for(let i=1;  i<COVER[municipio].length; i++) {
      let x = COVER[municipio][i];
      if (x.substr(0,prefix.length) != prefix)
        prefix=prefix.substr(0,prefix.length-1);
    }
    COVER2[municipio] = {
      cprefix:prefix, // commom prefix
      cells:COVER[municipio].map( k => k.substr(prefix.length) )
    }
  }
  console.log(JSON.stringify(COVER2));
}

function convStdTable(x) {
  // STD = 0'municipio', 1'ref', 2'celula_b4h', 'nivel', 'notas'
  if (x[0][0]=='municipio')
    x.shift();  //  drops CSV header
  let current = null; // municipio
  let lst = {};
  for (let i of x) {
    if (i[0]!=current) lst[ current=i[0] ] = [];
    let cell = i[2];
    if (cell.charAt(0)=='x') cell = cell.substr(1)
    if (cell) lst[current].push(cell);
  }
  return lst
}
